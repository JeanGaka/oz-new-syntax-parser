hvariablei ::= (uppercase char) { (alphanumeric char) }
| ’‘’ {h variableChari|h pseudoChari} ’‘’
hatomi ::= (lowercase char) { (alphanumeric char) } (except no keyword)
| ’’’ {h atomChari|h pseudoChari} ’’’
hstringi ::= ´"´ {h stringChari| hpseudoChari} ´"´
hcharacteri ::= (any integer in the range 0 ... 255)
| ´&´ hcharChari| ´&´ hpseudoChari