functor
import Browser(browse:Browse)
define B1 B2 NewActive BallGame in
	fun {NewActive Class Init}
		Obj={New Class Init}
		P
	in
		thread S in
			{NewPort S P}
			for M in S do {Obj M} end
		end
		proc {$ M} {Send P M} end
	end
	class BallGame
		attr other count:0

		meth init(Other)
			other:=Other
		end
		meth ball
			count:=@count+1
			{@other ball}
		end
		meth get(X)
			X=@count
		end
	end
	B1={NewActive BallGame init(B2)}
	B2={NewActive BallGame init(B1)}
	{B1 ball}
	local X in {B1 get(X)} {Browse X} end
end