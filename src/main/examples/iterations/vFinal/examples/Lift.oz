functor
import Browser(browse:Browse) OS NewPortObject
define FL Lift Building Controller Timer in
%%%% Implementation of the timer and controller components  START%%%%
fun {Timer}
	{NewPortObject
	  proc {$ Msg}
		  case Msg of starttimer(T Pid) then
			  thread {Delay T} {Send Pid stoptimer} end
		  end
	  end}
end
fun {Controller Init}
	Tid={Timer}
	Cid={NewPortObject Init
			fun {$ Msg state(Motor F Lid)}
				case Motor
				of running then
					case Msg
					of stoptimer then
                       {Send Lid 'at'(F)}
						state(stopped F Lid)
					end
				[] stopped then
					case Msg
					of step(Dest) then
						if F==Dest then
							 state(stopped F Lid)
						elseif F<Dest then
							 {Send Tid starttimer(5000 Cid)}
							 state(running F+1 Lid)
						else % F>Dest
							 {Send Tid starttimer(5000 Cid)}
							 state(running F-1 Lid)
						end
					end
				end
			end}
in Cid end
%%%% Implementation of the timer and controller components  END%%%%
%%%% Implementation of the ﬂoor component  START%%%%
fun {Floor Num Init Lifts}
	Tid={Timer}
	Fid={NewPortObject Init
			fun {$ Msg state(Called)}
				case Called
				of notcalled then Lran in
					case Msg
					of arrive(Ack) then
						{Browse 'Lift at floor '#Num#': open doors'}
						{Send Tid starttimer(5000 Fid)}
						state(doorsopen(Ack))
					[] call then
						{Browse 'Floor '#Num#' calls a lift!'}
						Lran=Lifts.(1+{OS.rand} mod {Width Lifts})
						{Send Lran call(Num)}
						state(called)
					end
				[] called then
					case Msg
					of arrive(Ack) then
						{Browse 'Lift at floor '#Num#': open doors'}
						{Send Tid starttimer(5000 Fid)}
						state(doorsopen(Ack))
					[] call then
						state(called)
					end
				[] doorsopen(Ack) then
					case Msg
					of stoptimer then
						{Browse 'Lift at floor '#Num#': close doors'}
						Ack=unit
						state(notcalled)
					[] arrive(A) then
						A=Ack
						state(doorsopen(Ack))
					[] call then
						state(doorsopen(Ack))
					end
				end
			end}
in Fid end

%%%% Implementation of the ﬂoor component  END
%%%% Execution %%%%
proc {Building FN LN ?Floors ?Lifts}
	Lifts={MakeTuple lifts LN}
	for I in 1..LN do Cid in
		 Cid={Controller state(stopped 1 Lifts.I)}
		 Lifts.I={Lift I state(1 nil false) Cid Floors}
	end
	Floors={MakeTuple floors FN}
	for I in 1..FN do
		 Floors.I={Floor I state(notcalled) Lifts}
	end
end
local  F L in
    {Building 20 2 FL }
    {Send F.20 call}
    {Send F.4 call}
    {Send F.10 call}
    {Send L.1 call(4)}
end
end