functor
import Browser(browse:Browse)
define C Fibo NewStat ComputeServer in
	fun {NewStat Class Init}
	P Obj={New Class Init} in
		thread S in
			 {NewPort S P}
			 for M#X in S do
				 try {Obj M} X=normal
				 catch E then X=exception(E) end
			 end
		end
		proc {$ M}
		X in
			 {Send P M#X}
			 case X of normal then skip
			 [] exception(E) then raise E end end
		end
	end
	class ComputeServer
		meth init skip end
		meth run(P) {P} end
	end
	C={NewStat ComputeServer init}
	fun {Fibo N}
		if  N<2 then 1 else {Fibo N-1}+{Fibo N-2} end
	end
	local F in
		{C run(proc {$} F={Fibo 30} end)}
		{Browse F}
	end
	local F in
		F={Fibo 30}
		{Browse F}
	end
end