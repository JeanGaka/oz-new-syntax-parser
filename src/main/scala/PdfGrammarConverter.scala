import java.awt.Toolkit
import java.awt.datatransfer.StringSelection

import parser.solution.newOzParser.elems.Keywords

import scala.io.Source
import scala.util.parsing.combinator.RegexParsers

object PdfGrammarConverter extends   RegexParsers with  Keywords {
  def main(args: Array[String]): Unit = {
    var s: String = Source.fromFile("idk.oz").getLines.mkString("\n")
    s = s.replaceAll( "([^\\(])h([^ \n]+)i([^\\)])","$1<$2>$3")
    s = s.replaceAll( "([^\\(])h ([^ \n]+)i([^\\)])","$1<$2>$3")
//    s = s.replaceAll( "α","\\\\alpha")
    s = s.replaceAll( "´([^´]+)´","`$1'")
    s = s.replaceAll( "([^`])\\{([^'])","$1\\\\{$2")
    s = s.replaceAll( "([^`])\\[([^'])","$1\\\\lbrack$2")
    s = s.replaceAll( "([^`])\\}([^'])","$1\\\\}$2")
    s = s.replaceAll( "([^`])\\]([^'])","$1\\\\rbrack$2")
    s = s.split("\n").map{
      l =>
        if(l.startsWith("""|""")){
          l.replaceFirst( "(\\|)","\\\\alt")
        }else if(l.startsWith("""<""")){
          "\n" + l
        }else{
          "\\\\ " + l
        }
    }.mkString("\n")

//    s = s.replaceAll( "h([^ ]+)i","<$1>")
    s = "\\begin{grammar}\n\n" + s + "\n\n\\end{grammar}"
    println(s)
    Toolkit.getDefaultToolkit
      .getSystemClipboard
      .setContents(
        new StringSelection(s),
        null
      )
    println("Copied to clipboard !")
  }
}
