import java.io.File
import java.util.concurrent.TimeUnit

import parser.solution.NewOzParserObj

import scala.io.Source
import scala.sys.process._

object NewOzCompiler {
  val exampleDirInput = "src/main/examples/iterations"
  val exampleDirOutput = "src/main/examples/iterations"

  val version = "v3"
  var inputFolder: String = exampleDirInput + "/" + version + "/input/"
  var outputFolder: String = exampleDirInput + "/" + version + "/output/"
  val folderArgs: List[String] = List("-f", "--folder")
  val verboseArgs: List[String] = List("-v", "--verbose")
  val forceArgs: List[String] = List("-F", "--force")
  val cleanArgs: List[String] = List("-C", "--clean")
  val keepArgs: List[String] = List("-k", "--keep")
  val runArgs: List[String] = List("-r", "--run")
  //TODO  : add  - o option
  // List of arguments to not pass to the oz compiler
  val ignoreList: List[String] = List(folderArgs,forceArgs,keepArgs,cleanArgs,runArgs).flatten
  def getListOfFiles(dir: String): List[File] = {
    val d = new File(dir)
    if (d.exists && d.isDirectory) {
      d.listFiles.filter(_.isFile).toList
    } else {
      List[File]()
    }
  }

  def extensor(orig: String, ext: String) = (orig.split('.') match {
    case xs@Array(x) => xs
    case y => y.init
  }) :+ "oz" mkString "."

  def main(args: Array[String]): Unit = {
//    args.foreach(println)
    if (args.length == 0) {
      println("Illegal Arguments")
    }
    // Printing help if required
    if (args.exists(List("-h", "-?", "--help").contains(_))) {
      println("Usage(for newOz coompiler).")
      println(f"${cleanArgs.mkString(",")}%s Clean the intermediary files (default mode).")
      println(f"${keepArgs.mkString(",")}%s Keeps the intermediary files.")
      println(f"${folderArgs.mkString(",")}%s Input and Output Folder to use (current folder if nothing),\n\t\twill compile all the files inside.")
      println(f"${forceArgs.mkString(",")}%s Displays newOz compiler message.")
      println(f"${verboseArgs.mkString(",")}%s Displays newOz compiler message.")
      println(f"${runArgs.mkString(",")}%s Runs the first (left-most) newOz file it with ozengine command.")
      System.exit(0)
    }
    // Managin folder usage
    val verbose: Boolean = args.exists(verboseArgs.contains(_))
    val nozfiles: List[File] = args.filter(scala.reflect.io.File(_).isFile).map(new File(_)).toList
    val usingFolders: Boolean = args.exists(folderArgs.contains(_))|| nozfiles.isEmpty
    inputFolder = "./"
    outputFolder = "./"
    if (usingFolders ) {
      if(verbose)
        println("Using directories")
      val dirs = args.filter(scala.reflect.io.File(_).isDirectory)
      if (dirs.length >= 1) {
        inputFolder = dirs(dirs.length - 1)
        inputFolder = inputFolder + (if (inputFolder.endsWith("/")) "" else "/")
      }
      outputFolder = inputFolder
      if (verbose)
        println("input folder", inputFolder)
      if (verbose)
        println("output folder", outputFolder)
    } else {

    }
    val files: List[File] = if (usingFolders) getListOfFiles(inputFolder) else nozfiles
    val inFiles: List[File] = files.filter(d => d.toString.endsWith(".noz"))
    if(inFiles.isEmpty){
      println("No files found/given")
      System.exit(-1)
    }
    if(verbose)
      println("Files to compile :\n\t" + inFiles.mkString("\n\t"))
    var counter = 0
    var createdFiles:List[String] = List() // not using mutable list for easy java compilation
    val start = System.nanoTime()
    for (filename <- inFiles) {
      val fileContents: String = Source.fromFile(filename).getLines.mkString("\n")
      val header = "=" * 60
      if (verbose)
        println(f"$header%s ${filename.getName}%s $header%s ")
      if (verbose)
        println(fileContents)
      val outputFileName:String = outputFolder + extensor(filename.getName, "oz")
      //Parses the code and prints it into a file
      if (NewOzParserObj.parseContent(fileContents, outputFileName)) {
        if (verbose)
          println("======== Converted    ========")
        counter = counter + 1
        createdFiles ::= outputFileName
        if (verbose)
          println(Console.GREEN + Console.BOLD + "OK")
      } else {
        if (verbose)
          println(Console.RED + Console.BOLD + "KO")
      }
      if (verbose)
        println(Console.RESET)
    }
    val end = System.nanoTime()
    val difference = end - start
    val result = f"($counter%d/${inFiles.length}%d)"
    if (verbose) {
      println("Total execution time: " +
        TimeUnit.NANOSECONDS.toHours(difference) + " hours " +
        ( TimeUnit.NANOSECONDS.toMinutes(difference) -  TimeUnit.HOURS.toMinutes(TimeUnit.NANOSECONDS.toHours(difference)))   + " min " +
        ( TimeUnit.NANOSECONDS.toSeconds(difference) -  TimeUnit.MINUTES.toSeconds(TimeUnit.NANOSECONDS.toMinutes(difference))) + " sec " +
        " - " + difference + " nSec (Total)")
      if (counter == inFiles.length)
        println(Console.GREEN + Console.BOLD + f"All content properly parsed $result%s !")
      else if (counter > 0)
        println(Console.YELLOW + Console.BOLD + f"Some files were not properly parsed $result%s !")
      else if (counter > 0)
        println(Console.RED + Console.BOLD + f"Some files were not properly parsed $result%s !")
      println(Console.RESET)
    }

    if(counter/inFiles.length != 1){
      println(f"Some errors occurred during the newOz compilation.\n Use ${forceArgs.mkString(",")}%s to force a result")
      // Brute force leave if no forcing
      if(!args.exists(forceArgs.contains(_))){
        System.exit(-1)
      }
    }
    val cmd = "ozc " + args.filter(a => !ignoreList.contains(a) &&
      !scala.reflect.io.File(a).isDirectory &&
    !scala.reflect.io.File(a).isFile).mkString(" ") + " " +  createdFiles.mkString(" ")
    println(cmd)
    val output = cmd.!!
    println(output)
    // Cleans if needed
    if(!args.exists(keepArgs.contains)){
      if(verbose)
        println("Cleaning generated intermediary files")
      createdFiles.map(new File(_)).filter(_.exists()).foreach(_.delete())
    }

    if(args.exists(runArgs.contains) && createdFiles.length >= 1){
      val f = createdFiles.head.split("/")

      val cmd2 = "ozengine " + "./" + f(f.length -1)  +"f" // add f to turn oz into ozf
      println(cmd2)
      val output2 = cmd2.!!
      println(output2)
    }
  }
}
