package parser.solution.newOzParser.elems

import java.util.concurrent.TimeUnit

import scala.util.parsing.combinator.RegexParsers

trait Util extends RegexParsers with Keywords{

  // Comments
  def oneLineComment: Parser[String] = commentOpt ~ """[^\n\r]*""".r ^^ {
    case opt ~ comment => translate(opt) + comment // We don't want to translate the comment itself
  }

  def multiLineComment: Parser[String] = "(/\\*(?:.|[\\n\\r])*?\\*/)".r ^^ (concat(_))

  def comments: Parser[String] = oneLineComment | multiLineComment
  def sep: Parser[String] = realSep ~ (comments ~ realSep ^^ (concat(_))).* ^^ (concat(_))
  def sepStrict: Parser[String] = realSep ~ (comments ~ realSep ^^ (concat(_))).* ^^ (concat(_))

  // Default separator parser
  def realSep: Parser[String] = whiteSpace.? ^^ (s => "" + s.getOrElse(""))
  def realSepStrict: Parser[String] = whiteSpace ^^ (s => s)

  def skip: Parser[Any] = "\n" | "\t"
  // Basic dictionary for things straight forward to translate
  val tr: Map[String, String] = Map(neq -> "\\=",
    time -> "*"
    , atomDelimiter -> lispOpt
    , closingBlockOpt -> "end"
    , elseifOpt -> "elseif"
    , caseOpt -> "[]"
    , rightCaseArrow -> "then"
    , matchOpt -> "case"
    , andthenOpt -> "andthen"
    , orelse -> "orelse"
    , leftPar -> "{"
    , rightPar -> "}"
    , defprocOpt -> "proc"
    , defOpt -> "fun"
    , extendsOpt -> "from"
    , fromOpt -> "at"
    , commentOpt -> "%%"
//    , "browse" -> "Browse"
    , thisOpt -> selfOpt
    , nilOpt -> "nil"
    , sendKw -> "Send"
    , newPortKw -> "NewPort"
    , newKw -> "New"
    , newMod -> mod
    , listOp -> pipe
  )

  // Return a space if the string is empty
  def orSpace(s: String): String = if(s.isEmpty) " " else s
  def trans(key: String): String = {
    tr.getOrElse(key, key)
  }

  /**
    * Unbox and concatenates the objects given in parameter
    *
    * @param elems the list of items to translate and concatenate
    * @return
    */
  def concat(elems: Any*): String = {
    if (elems == null)
      return ""
    elems.map {
      case null => ""
      case None => ""
      case s: String => s
      case c: Char => "" + c
      case e: Parser[Any] => e.toString()
      case list: List[Any] => list.mkString(" ")
      case e1 ~ e2 => concat(e1,e2)
      case option: Option[Any] => "" + concat(option.getOrElse(""))
    }.mkString("")
  }

  /**
    * Translate and concatenates the objects given in parameter
    *
    * @param elems the list of items to translate and concatenate
    * @return
    */
  def translate(elems: Any*): String = {
    if (elems == null)
      return ""
    elems.map {
      case null => ""
      case None => ""
      case s: String => trans(s)
      case c: Char => trans("" + c)
      case e: Parser[Any] => trans(e.toString())
      case list: List[Any] => list.mkString(" ")
      case e1 ~ e2 => translate(e1,e2)
      case option: Option[Any] => "" + translate(option.getOrElse(""))
    }.mkString("")
  }

  /**
    * Remove left recursion , not used in the actual code for performances (even if duplicaiton)
    * Used for testing to see if removing a left recusrions could help.
    * @param leftRecc where the recursion is (direct or indirect
    * @param terminal terminal expression
    * @return
    */
  def removeLeftRecursion(leftRecc: Parser[String], terminal: Parser[String]): Parser[String] = {
    def solPrime: Parser[String] = terminal ~ (sep ~ solPrime ^^ (translate(_))).? ^^ (translate(_))
    def sol: Parser[String] = (leftRecc ~ sep ~ terminal ~ (sep ~ solPrime ^^ (translate(_))).? ^^ (translate(_))) | solPrime
    sol
  }

  /**
    * Mesures the performances of f over the range r
    * @param r  range
    * @param f function to be applyed on each elements of the range
    */
  def mesurePerformance(r: Range.Inclusive, f: Int => Boolean): Unit = {
    // usage example  mesurePerformance( 1 to 100 , X => parseContent("{val A;}"))
    val start = System.nanoTime()
    r foreach (x => f(x))
    val end = System.nanoTime()
    val difference = end - start
    println("Total execution time: " +
      TimeUnit.NANOSECONDS.toHours(difference) + " hours " +
      (TimeUnit.NANOSECONDS.toMinutes(difference) - TimeUnit.HOURS.toMinutes(TimeUnit.NANOSECONDS.toHours(difference))) + " min " +
      (TimeUnit.NANOSECONDS.toSeconds(difference) - TimeUnit.MINUTES.toSeconds(TimeUnit.NANOSECONDS.toMinutes(difference))) + " sec " +
      " - " + difference + " nSec (Total)")
  }
}
