package parser.solution.newOzParser.elems

import scala.util.parsing.combinator.RegexParsers

trait Tokens extends RegexParsers with Keywords with Util {

  def bindigit: Parser[String] = "1" | "0" ^^ (concat(_))

  def nzdigit: Parser[String] = """[1-9]""".r ^^ (concat(_))

  def octDigit: Parser[String] = """[0-7]""".r ^^ (concat(_))

  def digit: Parser[String] = """\d""".r ^^ (concat(_))

  def hexDigit: Parser[String] = digit | """[a-f]""".r | """[A-F]""".r ^^ (concat(_))

  //  def integer: Parser[String] = """(~?0|[1-9]\d*)""".r ^^ (_.toString)
  def integer: Parser[String] = (("~".? ~ nzdigit ~ digit.*) ^^ (concat(_))) |
    (("~".? ~ "0" ~ octDigit.*) ^^ (concat(_))) |
    (("~".? ~ ("0x" | "0X") ~ hexDigit.+) ^^ (concat(_))) |
    (("~".? ~ ("0b" | "0B") ~ bindigit.+) ^^ (concat(_)))

  def float: Parser[String] = """(~?\d+\.\d*([eE]~?\d+)?)""".r ^^ (_.toString)

  def pseudoChar: Parser[String] = ("\\" ~ octDigit ~ octDigit ~ octDigit |
    ("\\x" | "\\X") ~ hexDigit ~ hexDigit | "\\\\" |
    "\\a" | "\\b" | "\\f" | "\\n" | "\\r" | "\\t" | "\\b" | "\\" + atomDelimiter |
    "\\" + atomDelimiter | "\\\"" | "\\" + variableDelimiter | "\\" + ozCharacter |
    "\\\"") ^^ (concat(_))

  def charChar: Parser[String] = guard(not("\\" | NUL)) ~ """(\S|\s)""".r ^^ {
    case g ~ s => s.toString
  }

  def variableChar: Parser[String] = guard(not(variableDelimiter | "\\" | NUL)) ~ """(\S|\s)""".r ^^ {
    case g ~ s => s.toString
  }

  def atomChar: Parser[String] = guard(not(atomDelimiter | "\\" | NUL)) ~ """(\S|\s)""".r ^^ {
    case g ~ s => s.toString
  }

  def stringChar: Parser[String] = guard(not(stringDelimiter | "\\" | NUL)) ~ """(\S|\s)""".r ^^ {
    case g ~ s => s.toString
  }

  def string: Parser[String] = stringDelimiter ~ (pseudoChar | stringChar).* ~ stringDelimiter ^^ {
    case d1 ~ strings ~ d2 => concat(translate(d1), strings.mkString(""), translate(d2))
  }

  def ozCharacterPrefix: Parser[String] = "&"

  def characterBetween1And255 : Parser[String] = "2[0-4][0-9]".r | "25[0-5]".r  | "1?[0-9]{1,2}".r  ^^ (_.toString)
  def ozCharacter: Parser[String] = characterBetween1And255 |
    (ozCharacterPrefix ~ (charChar|pseudoChar) ^^ {
      case prefix ~ content => concat(translate(prefix),content)
    })

}
