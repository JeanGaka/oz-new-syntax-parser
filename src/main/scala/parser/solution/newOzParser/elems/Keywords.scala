package parser.solution.newOzParser.elems

import scala.util.parsing.combinator.RegexParsers

trait Keywords extends RegexParsers {
  // Symbols
  val mod = "mod"
  val newMod = "%"
  val eq = "=="
  // Operators
  val andthenOpt = "&&"
  val attrOpt = "attr"
  val caseOpt = "case"
  val matchOpt = "match"
  val catchOpt = "catch"
  val choiceOpt = "choice"
  val classOpt = "class"
  val condOpt = "cond"
  val declareOpt = "declare"
  val defineOpt = "define"
  val disOpt = "dis"
  val divOpt = "div"
  val doOpt = "do"
  val elseOpt = "else"
  val elsecaseOpt = "elsecase"
  val elseifOpt = "else if"
  val closingBlockOpt = "}"
  val commentOpt = "//"
  val exportOpt = "export"
  val failOpt = "fail"
  val falseOpt = "false"
  val featOpt = "feat"
  val finallyOpt = "finally"
  val forOpt = "for"
  val fromOpt = "from"
  val extendsOpt = "extends"
  val defOpt = "def"
  val functorOpt = "functor"
  val ifOpt = "if"
  val importOpt = "import"
  val inOpt = "in"
  val lazyOpt = "lazy"
  val localOpt = "local"
  val lockOpt = "lock"
  val lispOpt = "'"
  val methOpt = "meth"
  val modOpt = "mod"
  val notOpt = "not"
  val ofOpt = "of"
  val orOpt = "or"
  val orelse = "||"
  val prepareOpt = "prepare"
  val defprocOpt = "defproc"
  val propOpt = "prop"
  val raiseOpt = "raise"
  val requireOpt = "require"
  val selfOpt = "self"
  val thisOpt = "this"
  val thenOpt = "then"
  val skipOpt = "skip"
  val openningBlockOpt = "{"
  val threadOpt = "thread"
  val trueOpt = "true"
  val tryOpt = "try"
  val unitOpt = "unit"
  val varOpt = "var"
  val valOpt = "val"

  // Operator symbols
  val at = "@"
  val atomDelimiter = "´"
  val brackets = "[]"
  val bangOpt = "!"
  val caret = "^"
  val colon = ":"
  val colonColon = "::"
  val colonColonColon = ":::"
  val comma = ","
  val div2 = "/"
  val divColon = "\\=:"
  val dollar = "$"
  val dot = "."
  val doubleQuotes = "\"\""
  val colonEqual = ":="
  val doubleExclamationMark = "!!"
  val assign = "="
  val equalColon = "=:"
  val equalEqual = "=="
  val exclamationMark = "!"
  val gt = ">"
  val gtColon = ">:"
  val gte = ">="
  val gteColon = ">=:"
  val hashOpt = "#"
  val leftArrow = "<-"
  val rightCaseArrow = "=>"
  val leftBracket = "["
  val leftCurlyBracket = "{"
  val leftPar = "("
  val lt = "<"
  val ltColon = "<:"
  val lte = "<="
  val lte2 = "=<"
  val lteColon = "=<:"
  val minus = "-"
  val NUL = "\\0"
  val neq = "!="
  val listOp = "::"
  val pipe = "|"
  val plus = "+"
  val rightBracket = "]"
  val rightCurlyBracket = "}"
  val rightPar = ")"
  val twoDots = ".."
  val stringDelimiter = "\""
  val threeDots = "..."
  val tildeOpt = "~"
  val time = "*"
  val variableDelimiter = "`"
  val underscore = "_"
  val newCell = "NewCell"
  val nilOpt = "nil"


  // For loop idioms
  val semiColonOpt = ";"
  val continueOpt = "continue"
  val breakOpt = "break"
  val defaultOpt = "default"
  val returnOpt = "return"
  val collectOpt = "collect"
  // others not always defined as keyword in the book
  val sendKw = "send"
  val newPortKw = "newPort"
  val newKw = "new"
  val endOpt = "end"
  val nullOpt = "null"

  // Method head
  val defaultArgOpt = "="

  def loopDecBaseIdiom: Parser[String] = breakOpt | continueOpt | returnOpt | collectOpt

  def loopDecBaseExtraIdiom: Parser[String] = defaultOpt

  // All keyword , preferable to be ordered by usage frequence
  def keyword: Parser[String] = functorOpt | loopDecBaseIdiom | loopDecBaseExtraIdiom |
    valOpt |varOpt| attrOpt | propOpt | ifOpt | elseifOpt | elsecaseOpt |elseOpt | openningBlockOpt | importOpt |
  orelse |andthenOpt | threadOpt  | mod | endOpt | importOpt | "andthen" | "orelse" | trueOpt | falseOpt |
    doOpt/* | attrOpt | caseOpt | matchOpt | catchOpt |
    choiceOpt | classOpt | condOpt | declareOpt | defineOpt | disOpt | divOpt | doOpt | elseOpt |
    elsecaseOpt | elseifOpt | closingBlockOpt | commentOpt | exportOpt | failOpt | falseOpt |
    featOpt |  finallyOpt | forOpt | fromOpt | extendsOpt | defOpt | functorOpt | ifOpt |
    ofOpt |  orOpt | orelse | prepareOpt | defprocOpt | propOpt | raiseOpt | requireOpt |
    selfOpt | openningBlockOpt | threadOpt | trueOpt | tryOpt | unitOpt |
//    varOpt |  valOpt | at | atomDelimiter | brackets | caret | colon | colonColon |
//    colonColonColon |  comma | div2 | divColon | dollar | dot | doubleQuotes | colonEqual |
//    doubleExclamationMark |  assign | equalColon | equalEqual | exclamationMark | gt | gtColon |
//    gte | gteColon |  hashOpt | leftArrow | rightCaseArrow | leftBracket | leftCurlyBracket |
//    leftPar | lt |  ltColon | lte | lte2 | lteColon | minus | NUL | neq | pipe | plus |
//    rightBracket |  rightCurlyBracket | rightPar | twoDots | stringDelimiter | threeDots |
//    tildeOpt |  time | variableDelimiter | newCell | underscore/*| nilOpt*/ | semiColonOpt |
    continueOpt |  breakOpt | defaultOpt | returnOpt | thenOpt | bangOpt|
    importOpt | lazyOpt | localOpt | lockOpt | lispOpt | methOpt | modOpt | notOpt |  // in opt slows down too much*/
    // the performances|
    // inOpt
    collectOpt ^^ (_.toString)

  // Special keywords to be kept as and used as variables
  def specialKeyword: Parser[String] = nilOpt  | nullOpt | thisOpt | newPortKw | newKw | sendKw | skipOpt
}
