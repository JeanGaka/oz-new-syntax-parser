package parser.solution.newOzParser.enums

object NestableType extends Enumeration {
  type Alpha = Value
  val EXPRESSION, STATEMENT, EXPRESSION_OR_STATEMENT, VARIABLE, DOLLAR, METHOD_HEAD, PATTERN = Value
}
