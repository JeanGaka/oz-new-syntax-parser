package parser.solution

import java.io.PrintWriter

import parser.solution.newOzParser.enums.NestableType._
import parser.solution.newOzParser.elems.{Keywords, Tokens, Util}

import scala.util.matching.Regex
import scala.util.parsing.combinator.RegexParsers
import scala.util.parsing.input.CharSequenceReader

class NewOzParser extends RegexParsers with Keywords with Tokens with Util {

  //  override val whiteSpace = """(\s|\n)+""".r
  override val whiteSpace: Regex =
    """(\s|\n|\r)+""".r
  // Specific token
  val functionParamOpen: String = leftPar
  val functionParamEnd: String = rightPar

  def escapedVariable: Parser[String] = (variableDelimiter ~ (variableChar | pseudoChar).+ ~ variableDelimiter) ^^ {
    case d1 ~ content ~ d2 => concat(translate(d1), content.mkString(""), translate(d2))
  }

  override def skipWhitespace: Boolean = false

  // Patterns
  def variable: Parser[String] =
    (guard(not(keyword)) ~ """([a-zA-Z](\w|\d)*)""".r ^^ {
      case _ ~ v =>
        if (parseAll(specialKeyword, v).successful) {
          translate(v)
        } else {
          if (v.charAt(0).isLower) variableDelimiter + v + variableDelimiter else v.toString
        }
    }) | escapedVariable


  def dollarParser: Parser[String] = dollar

  def atomLips: Parser[String] = lispOpt ~ guard(not(keyword)) ~"""([a-zA-Z](\w|\d)*)""".r ^^ { case _ ~ _ ~ v =>
    if (v.charAt(0).isUpper & ("" + v.charAt(0)) != atomDelimiter) // upper case variables should be delimited
      translate(atomDelimiter) + v + translate(atomDelimiter)
    else
      v
  }


  def atomLipsMeth: Parser[String] = lispOpt ~ guard(not(keyword)) ~"""([a-z](\w|\d)*)""".r ^^ { case _ ~ _ ~ v =>
    if (v.charAt(0).isUpper & ("" + v.charAt(0)) != atomDelimiter) // upper case variables should be delimited
      translate(atomDelimiter) + v + translate(atomDelimiter)
    else
      v
  }

  def escapedAtom: Parser[String] = (atomDelimiter ~ (atomChar | pseudoChar).+ ~ atomDelimiter) ^^ {
    case d1 ~ content ~ d2 => concat(translate(d1), content.mkString(""), translate(d2))
  }

  def atom: Parser[String] = atomLips | escapedAtom

  // either lisp atom , uppercase variable or any of them escaped
  def methodHeadName: Parser[String] = atomLipsMeth |
    (guard(not(keyword)) ~ "([A-Z](\\w|\\d)*)".r ^^ {
      case _ ~ name => name
    }) | escapedVariable | escapedAtom

  // Operators
  def unaryOperator: Parser[String] = tildeOpt ^^ (translate(_))

  def binaryOperator: Parser[String] = evalBinOperator | consBinOperator ^^ (concat(_))

  def evalBinOperator: Parser[String] = (dot | plus | minus | time | divOpt | div2 | newMod |
    eq | neq | lte | lt | gte | gt | andthenOpt | orelse) ^^ (translate(_))

  // There is an error in the thesis where the lists operator wasn't properly updated in the appendix,
  // Allowing both for now
  def consBinOperator: Parser[String] = (hashOpt | listOp | pipe) ^^ (translate(_))

  //  Data structures and patterns
  def feature: Parser[String] = atom | integer | trueOpt | falseOpt | unitOpt

  def label: Parser[String] = atom | trueOpt | falseOpt | unitOpt // | variable | atom


  def bangVariable: Parser[String] = bangOpt ~ variable ^^ {
    case opt ~ v => concat(translate(opt), v)
  }


  def labelFeats(t: Alpha): Parser[String] = feature ~ sep ~ colonNest(t) ^^ (concat(_))

  def labelCall: Parser[String] = label ~ `leftPar` ~ repsep(sep ~ (labelFeats(EXPRESSION) | nested(EXPRESSION)) ~ sep ^^ (concat(_)),
    comma) ~ sep ~ rightPar ^^ (concat(_))

  def commaThreeDots: Parser[String] = comma ~ sep ~ threeDots ^^ (translate(_))

  def labelCallForPattern: Parser[String] = label ~ `leftPar` ~ repsep(sep ~ (labelFeats(PATTERN) | nested(PATTERN)) ~ sep ^^ (concat(_))
    , comma) ~ ((sep ~ comma ~ sep ~ threeDots) ^^ {
    case sep1 ~ _ ~ sep2 ~ dots => translate(sep1, sep2, dots)
  }).? ~ sep ~ rightPar ^^ (concat(_))

  def arrayNest(t: Alpha): Parser[String] = leftBracket ~ sep ~ nonEmptyParameters(t) ~ sep ~ rightBracket ^^ {
    case lb ~ sep1 ~ params ~ sep2 ~ rb => concat(translate(lb, sep1), params, translate(sep2, rb))
  }

  def consBinExpression: Parser[String] = expression ~ sep ~ consBinOperator ~ sep ~ expression ^^ {
    case exp1 ~ sep1 ~ op ~ sep2 ~ exp2 => concat(exp1, translate(sep1, op, sep2), exp2)
  }

  def term: Parser[String] = binaryConsExp | termP

  def termP: Parser[String] = (bangVariable | labelCall | integer | float | ozCharacter | atom | string |
    trueOpt | falseOpt | unitOpt | arrayNest(EXPRESSION) | variable) ^^ (concat(_))

  def pattern: Parser[String] = binaryConsPattern | patternP

  def patternP: Parser[String] = bangVariable | labelCallForPattern | integer | float | ozCharacter | atom | string |
    trueOpt | falseOpt | unitOpt | arrayNest(PATTERN) | "nil" | variable

  def dataExpression: Parser[String] = (atom | atVariable | integer | float | string | ozCharacter | trueOpt |
    falseOpt | unitOpt) ^^
    (concat(_))

  // Conditional (IF,elseif , else)
  def betweenParExpression: Parser[String] = `leftPar` ~ sep ~ expression ~ sep ~ `rightPar` ^^ {
    case _ ~ sep1 ~ exp ~ sep2 ~ _ => concat(orSpace(sep1), exp, orSpace(sep2))
  }

  // For real oz parenthesis
  def betweenParInNested(t: Alpha): Parser[String] = `leftPar` ~ sep ~ nestedIn(t) ~ sep ~ `rightPar` ^^ {
    case lp ~ sep1 ~ exp ~ sep2 ~ rp => concat(lp, sep1, exp, sep2, rp)
  }

  def elseIfNest(t: Alpha): Parser[String] = `elseifOpt` ~ sep ~ betweenParExpression ~ sep ~ betweenBlockInNest(t) ^^ {
    case elifOp ~ sep1 ~ parExp ~ sep2 ~ codeB =>
      concat(translate(elifOp), sep1, parExp, " " + thenOpt, sep2, codeB)
  }

  def elseInNest(t: Alpha): Parser[String] = `elseOpt` ~ sep ~ (betweenBlockInNest(t) | nestedIn(t)) ^^ (translate(_))

  def ifNested(t: Alpha): Parser[String] = `ifOpt` ~ sep ~ betweenParExpression ~ sep ~ betweenBlockInNest(t) ~
    (sep ~ elseIfNest(t) ^^ (concat(_))).* ~ sep ~ elseInNest(t).? ^^ {
    case ifOp ~ sep1 ~ parExp ~ sep2 ~ codeB ~ eIfList ~ sep6 ~ optElse =>
      concat(translate(ifOp), sep1, parExp, " " + thenOpt, sep2, codeB, eIfList, sep6, optElse, if (!optElse
        .getOrElse("\n").endsWith("\n")) " " else null, translate(closingBlockOpt))
  }


  // Conditional (Switch cases)

  // Todo check for andthen
  def casePatternThenLeftPart: Parser[String] = `caseOpt` ~ sep ~ pattern ~ (sepStrict ~ `andthenOpt` ~ sep ~ expression ^^ (translate(_))).? ~ sep ~
    `rightCaseArrow` ^^ (translate(_))


  def casePatternThenNest(t: Alpha): Parser[String] = casePatternThenLeftPart ~ sep ~ (betweenBlockInNest(t) |
    nestedIn(t)) ~ sep ^^ (concat(_))


  def caseLeftPart: Parser[String] = (`matchOpt` | `condOpt`) ~ sep ~ pattern ~ sep ~ `openningBlockOpt` ^^ {
    case matchOp ~ sep1 ~ pat ~ sep2 ~ _ => concat(translate(matchOp), sep1, pat, sep2)
  }

  def caseNest(t: Alpha): Parser[String] = caseLeftPart ~ sep ~ casePatternThenNest(t).+ ~ (elseInNest(t) ~ sep ^^ (concat(_))).? ~ `closingBlockOpt` ^^ {
    case left ~ sep1 ~ casePatsList ~ elseInOpt ~ cBO =>
      val pat1 = {
        casePatsList.head.toString.replaceFirst("\\[\\]", ofOpt)
      }
      translate(left, sep1, pat1, " ", casePatsList.slice(1, casePatsList.size), elseInOpt, cBO)
  }


  // State and classes


  // Cell related
  def newCellExpression: Parser[String] = `varOpt` ~ sep ~ variable ~ sep ~ assign ~ sep ~ expression ^^ {
    case _ ~ _ ~ v ~ sep1 ~ a ~ sep2 ~ exp => translate(v, sep1, a, sep2, "{NewCell ", exp) + "}"
  }

  def atExpression: Parser[String] = `at` ~ expressionPrime ^^ (concat(_))

  def atVariable: Parser[String] = `at` ~ variable ^^ (concat(_))

  def cellModificationTerminal: Parser[String] = `colonEqual` ~ sep ~ expression ^^ (concat(_))

  def cellModificationPrime: Parser[String] = cellModificationTerminal ~ (sep ~ cellModificationPrime ^^ (translate(_))).? ^^ (translate(_))

  def cellModification: Parser[String] = (expressionPrime ~ sep ~ cellModificationTerminal ~ (sep ~ cellModificationPrime ^^ (translate(_))).? ^^ (translate(_))) | cellModificationPrime


  // todo check for  exp,exp and call variable
  def declarationPart: Parser[String] = (variable | pattern) ~ sep ~ assign ~ sep ~ (expression | statements) ^^
    (concat(_))

  // Values related
  def anyAssignementTerminal: Parser[String] = assign ~ sep ~ expression ^^ (concat(_))

  def anyAssignementPrime: Parser[String] = anyAssignementTerminal ~ (sep ~ anyAssignementPrime ^^ (translate(_))).? ^^ (translate(_))

  def anyAssignement: Parser[String] = (expressionPrime ~ sep ~ anyAssignementTerminal ~ (sep ~ anyAssignementPrime ^^ (translate(_))).? ^^ (translate(_))) | anyAssignementPrime


  def valueAssignment: Parser[String] = `valOpt` ~ rep1sep(sep ~ (variable | pattern) ~ (sep ~ assign ~ sep ~ (expression | statement)).? ~ sep ^^ {
    case sep1 ~ v ~ exp ~ sep2 => (concat(orSpace(sep1), v, exp), orSpace(sep2))
  }, comma) ~ sep ~ ";".? ^^ {
    case _ ~ variables ~ sep1 ~ _ =>
      // removing the very last separator
      concat(variables.map(v => concat(v._1, v._2)).dropRight(1), variables.map(v => concat(v._1)).last, " ")
  }

  def varAssignment: Parser[String] = `varOpt` ~ rep1sep(sep ~ (variable | pattern) ~ sep ~ assign ~ sep ~ (expression | statement) ~ sep ^^ {
    case sep1 ~ v ~ sep2 ~ a ~ sep3 ~ exp ~ sep4 =>
      (concat(orSpace(sep1), v, sep2, a, sep3, "{NewCell ", exp, "}"), orSpace(sep4))
  }, comma) ~ sep ~ ";".? ^^ {
    case _ ~ variables ~ sep1 ~ _ =>
      // removing the very last separator
      concat(variables.map(v => concat(v._1, v._2)).dropRight(1) + variables.map(v => concat(v._1)).last, sep1)
  }

  // Functional
  def parameters: Parser[String] = repsep((sep ~ expression ~ sep) ^^ {
    concat(_)
  }, ",") ^^ {
    translate(_)
  }

  def nonEmptyParameters(t: Alpha): Parser[String] = rep1sep((sep ~ nested(t) ~ sep) ^^ { case _ ~ v ~ _ => translate(v) }, ",") ^^ {
    translate(_)
  }


  /** *
    * Curried function meant to allow the seperation of expressions and parameters
    *
    * @param rhs what should apear on the  right of the Oz function call
    * @param lhs what should apear on the  left of the Oz function call
    * @return
    */
  def waitingForExp(rhs: Any)(lhs: Any): String = {
    translate(`leftPar`, lhs, " ", rhs, `rightPar`)
  }


  def functionCallTerminal: Parser[Any => String] = `leftPar` ~ sep ~ parameters ~ sep ~ `rightPar` ^^ {
    case leftP ~ _ ~ expList ~ _ ~ _ =>
      waitingForExp(expList)
  }

  // functionCall'
  def functionCallP: Parser[(Any => String, List[Any => String])] = functionCallTerminal ~ sep ~ functionCallP.? ^^ {
    case f ~ _ ~ exp =>
      if (exp.isDefined) {
        val nextParam: List[Any => String] = exp.get._2
        // Generates a list of all the function call in the reverse order
        (f, nextParam ++ List(f))
      } else {
        (f, List(f))
      }
  }

  def functionCall: Parser[Any] = ((binaryExpression2 | variable | atExpression | dataExpression) ~ sep ~ functionCallTerminal ~ sep.? ~ functionCallP.? ^^ {
    case exp1 ~ sep1 ~ term ~ sep2 ~ exp2 =>
      if (exp2.isDefined) {
        val l = exp2.get._2
        // Curying compatility
        // Applies each following function call from left to right to convert them in a bottom to top oz Call
        concat(l.scanRight(term(exp1)) { case (item, acc) => item(acc) }.head, sep1, sep2)
      } else {
        concat(term(exp1), sep1, sep2)
      }
  }) | functionCallP

  def procedureHead(t: Alpha): Parser[String] = `defprocOpt` ~ sep ~ nested(t) ~ sep ~ `leftPar` ~ sep ~ parameters ~ sep ~ `rightPar` ^^ {
    case op ~ sep1 ~ funName ~ _ ~ leftP ~ sep3 ~ expList ~ sep4 ~ rightP => {
      translate(op, sep1, leftP, funName + " ", orSpace(sep3), expList, sep4, rightP)
    }
  }

  def procedure(t: Alpha): Parser[String] = procedureHead(t) ~ sep ~ betweenBlockInNest(STATEMENT, hide = true) ^^ {
    case head ~ sep1 ~ exp => concat(head, sep1, exp, " end")
  }

  def lazyDenominator: Parser[String] = sep ~ lazyOpt ^^ (translate(_))

  def functionHead(t: Alpha): Parser[String] = `defOpt` ~ sep ~ lazyDenominator.? ~ sep.? ~ nested2(t) ~ sep ~ `leftPar` ~ sep ~ parameters ~ sep ~ `rightPar` ^^ {
    case op ~ sep0 ~ lazyOp ~ sep1 ~ funName ~ _ ~ leftP ~ sep3 ~ expList ~ sep4 ~ rightP =>
      translate(op, sep0, lazyOp, sep1, leftP, funName, orSpace(sep3), expList, sep4, rightP)
  }

  def function(t: Alpha): Parser[String] = functionHead(t) ~ sep ~ betweenBlockInNest(EXPRESSION, hide = true) ^^ {
    case head ~ sep1 ~ exp => concat(head, sep1, exp, " end")
  }

  def lambdaFunction: Parser[String] = `leftPar` ~ sep ~ parameters ~ sep ~ `rightPar` ~ sep.? ~ `rightCaseArrow`.? ~ sep ~ (betweenBlockInNest(EXPRESSION) | betweenBlockInNest(STATEMENT)) ^^ {
    case _ ~ sep1 ~ params ~ _ ~ _ ~ sep3 ~ arrow ~ _ ~ exp =>
      // if there is no arrow it is a proc , else a fun
      concat(if (arrow.isDefined) "fun" else "proc", " {$ ", sep1, params + "}", sep3, exp, " end")
  }

  // Threads and locks


  // Functors

  def atAtom: Parser[String] = `fromOpt` ~ sep ~ atom ~ sep ^^ {
    case fOpt ~ sep1 ~ a ~ _ => translate(fOpt, sep1, a)
  }

  def colonNest(t: Alpha): Parser[String] = `colon` ~ sep ~ nested(t) ^^ {
    case opt ~ sep1 ~ v => translate(opt, sep1, v)
  }

  def functorImportFeatParams: Parser[String] = rep1sep(sep ~ (atom | integer) ~ sep ~ colonNest(VARIABLE).? ~ sep ^^
    (concat(_)), comma) ^^ {
    case params => concat(params.mkString(" "))
  }

  def functorImportFeat: Parser[String] = (variable ~ sep ~ leftPar ~ functorImportFeatParams ~ rightPar) ^^
    (concat(_)) | (variable ~ sep ~ atAtom.?) ^^ {
    case v ~ sep1 ~ op => concat(v, if (op.isDefined) sep1 else "", op.getOrElse(""))
  }

  def functorExportFeat: Parser[String] = (atom | integer) ~ sep ~ colonNest(VARIABLE) ^^
    (translate(_))


  def functorImportsOrExports: Parser[(String, Boolean, String)] = rep1sep(((`importOpt` ~ sep ~ functorImportFeat) | (`exportOpt` ~ sep ~ (functorExportFeat | variable))) ^^ {
    case sign ~ sep1 ~ attr => (sign, concat(sep1, attr))
  }, sep) ^^ {
    values =>
      val imports = values.filter(_._1 == importOpt).map(_._2)
      val importsContent = if (imports.isEmpty) "" else concat(importOpt, imports)
      val exports = values.filter(_._1 == exportOpt).map(_._2)
      val exportsContent = if (exports.isEmpty) "" else concat(exportOpt, exports)
      (importsContent, exports.nonEmpty && imports.nonEmpty, exportsContent)
  }

  def functorDefine: Parser[String] = betweenBlockInNest(STATEMENT) ^^
    (default => translate(defineOpt, " ", default))

  def functor(t: Alpha): Parser[String] = `functorOpt` ~ sep ~ (nested(t) | sep) ~ (sep ~ functorImportsOrExports ^^ {
    case sep1 ~ importsExports =>
      val (l, cond, r) = importsExports
      concat(sep1, l, if (cond) sep1 else "", r)
  }).? ~ sep ~ betweenBlockInNest(STATEMENT, true) ^^ {
    case funcOpt ~ sep1 ~ funName ~ importsExports ~ sep4 ~ inStat =>
      concat(translate(funcOpt),
        sep1, funName, importsExports, if (sep4.isEmpty) sep1 else sep4, "define ", inStat, sep4, translate(closingBlockOpt))
  }


  // Exception

  def raiseExpression: Parser[String] = raiseOpt ~ sep ~ betweenBlockInNest(EXPRESSION) ^^ {
    case opt ~ sep1 ~ exp => translate(opt, sep1, exp, closingBlockOpt)
  }

  def finallyNest(t: Alpha): Parser[String] = finallyOpt ~ sep ~ betweenBlockInNest(t) ^^
    (translate(_))

  def catchLeftPart: Parser[String] = `catchOpt` ~ sep ~ `openningBlockOpt` ^^ {
    case matchOp ~ sep1 ~ _ => translate(matchOp, sep1)
  }

  def catchNest(t: Alpha): Parser[String] = catchLeftPart ~ sep ~ casePatternThenNest(t).+ ~ sep ~ `closingBlockOpt` ^^ {
    case left ~ _ ~ casePatsList ~ sep5 ~ cBO =>
      val pat1 = {
        casePatsList.head.toString.replaceFirst("\\[\\]", "")
      }
      concat(left, pat1, " ", casePatsList.slice(1, casePatsList.size), orSpace(sep5))
  }

  def exceptionNest(t: Alpha): Parser[String] = (tryOpt ~ sep ~ betweenBlockInNest(t) ~ (sep ~ catchNest(t) ^^ (concat(_))).? ~ (sep ~ finallyNest(t) ^^ (concat(_))).?) ^^ {
    case opt ~ sep1 ~ inStat ~ cS ~ fS =>
      concat(translate(opt), if (sep1.isEmpty) " " else sep1, inStat, cS, fS, if (sep1.isEmpty) " " else sep1, translate(closingBlockOpt))
  }

  // Loop

  def twoDotsExp: Parser[String] = sep ~ twoDots ~ sep ~ expression ^^ {
    case sep0 ~ opt ~ sep1 ~ exp => concat(sep0, translate(opt), sep1, exp)
  }

  def semiColonExp: Parser[String] = sep ~ semiColonOpt ~ sep ~ expression ^^ {
    case sep0 ~ opt ~ sep1 ~ exp => concat(sep0, translate(opt), sep1, exp)
  }


  def loopDec1: Parser[String] = (pattern | term | variable) ~ sep ~ inOpt ~ sep ~ expression ~ twoDotsExp.? ~ semiColonExp.? ^^ (concat(_))

  def loopDec2: Parser[String] = (pattern | term | variable) ~ sep ~ inOpt ~ sep ~ expression ~ semiColonExp ~ semiColonExp ^^ (concat(_))

  def loopDec3: Parser[String] = loopDecBaseIdiom ~ sep ~ colon ~ sep ~ variable ^^ (concat(_))

  def loopDec4: Parser[String] = loopDecBaseExtraIdiom ~ sep ~ colon ~ sep ~ expression ^^ (concat(_))

  def loopDec: Parser[String] = loopDec2 | loopDec1 | loopDec3 | loopDec4

  def forLoop(t: Alpha): Parser[String] = forOpt ~ sep ~ leftPar ~ sep ~ (loopDec ~ sep ^^ (concat(_))).+ ~ rightPar ~ sep ~ betweenBlockInNest(t, false, true) ^^ {
    case opt ~ sep1 ~ _ ~ sep2 ~ l ~ _ ~ sep4 ~ codeBlock =>
      concat(translate(opt), sep1, orSpace(sep2), sep2, l.mkString(""), orSpace(sep4), sep4, translate(doOpt), codeBlock, " end")
  }

  // Classes and methods

  def methHeadDefaultArg: Parser[String] = defaultArgOpt ~ sep ~ expression ^^ {
    case _ ~ sep1 ~ exp => concat(lte, sep1, exp)
  }

  def methHeadArg: Parser[String] = (labelFeats(METHOD_HEAD) |
    nested(METHOD_HEAD)) ~ (sep ~ methHeadDefaultArg).? ^^ (concat(_))

  def methHeadArgs: Parser[String] = repsep(sep ~ methHeadArg ~ sep ^^ {
    concat(_)
  }, comma) ^^ (concat(_))

  def methHeadSignature: Parser[String] = leftPar ~ sep ~ methHeadArgs ~ sep ~ commaThreeDots.? ~ sep ~ rightPar ^^ {
    case lp ~ sep1 ~ args ~ sep2 ~ dots ~ sep3 ~ rp =>
      if (args.isEmpty && dots.isEmpty) {
        concat("")
      } else {
        concat(lp, sep1, args, sep2, dots, sep3, rp)

      }
  }

  def methHeadAssign: Parser[String] = assign ~ sep ~ variable ^^ (concat(_))

  def methHead: Parser[String] = (bangVariable | methodHeadName /*| variable | atom */ | unitOpt | trueOpt | falseOpt) ~
    (sep ~ methHeadSignature).? ~ (sep ~ methHeadAssign).? ^^ (concat(_))

  def meth: Parser[String] = defOpt ~ sep ~ methHead ~ sep ~ betweenBlockInNest(EXPRESSION_OR_STATEMENT) ^^ {
    case _ ~ sep1 ~ head ~ sep2 ~ codeBlock => concat(methOpt, sep1, head, sep2, orSpace(codeBlock), endOpt)
  }

  def classExtendsExpression: Parser[String] = extendsOpt ~ sep ~ parameters ^^ (translate(_))

  def attrInit: Parser[String] = (bangVariable | variable | atom | unitOpt | trueOpt | falseOpt) ~
    (sep ~ colon ~ expression).? ^^ (concat(_))


  // Return a tuple(type, value) to be sorted/filtered by the parent
  def attrsOrProps: Parser[(String, String, String)] = (attrOpt | propOpt) ~ rep1sep(sep ~ attrInit ^^ {
    case sep1 ~ v => concat(sep1, v)
  }, sep ~ comma) ~ (sep ~ ";" ^^ { case s ~ _ => orSpace(s) }).? ~ sep ^^ {
    case sign ~ vs ~ sep1 ~ sep2 =>
      (sign, concat(vs, sep1), sep2)
  }


  def classAttributesOrProperties: Parser[(String, Boolean, String)] = rep1(attrsOrProps ~ sep ^^ {
    case values ~ sep1 => (values._1, concat(values._2), values._3)
  }) ^^ {
    values =>
      // Reconstruct values and props as intented
      val props = values.filter(_._1 == propOpt).map(_._2)
      val propsContent = if (props.isEmpty) "" else {
        val lastSep = values.filter(_._1 == propOpt).map(_._3).last
        concat(propOpt, props, orSpace(lastSep))
      }
      val attrs = values.filter(_._1 == attrOpt).map(_._2)
      val attrsContent = if (attrs.isEmpty) "" else {
        val lastSep = values.filter(_._1 == attrOpt).map(_._3).last
        concat(attrOpt, attrs, orSpace(lastSep))
      }
      (propsContent, attrs.nonEmpty && props.nonEmpty, attrsContent)

  }

  def classNest(t: Alpha): Parser[String] = `classOpt` ~ sep ~ nested(t) ~ (sep ~ classExtendsExpression).? ~ sep ~ openningBlockOpt ~
    (sep ~ classAttributesOrProperties ^^ {
      case sep1 ~ attrs =>
        val (props, cond, r) = attrs
        concat(sep1, props, if (cond) sep1 else "", r)
    }).? ~ sep ~ ((meth ~ sep) ^^ (translate(_))).* ~ closingBlockOpt ^^ {
    case op1 ~ sep1 ~ name ~ exts ~ sep2 ~ oBO ~
      attrsAndProps ~ sep4 ~ meths ~ cBO =>
      translate(op1, sep1, name, exts, orSpace(sep2),
        attrsAndProps, sep4, meths.mkString(""), cBO)
  }

  // Expression and statements

  def patternEqualExpression: Parser[String] = pattern ~ sep ~ assign ~ sep ~ expression ^^ (concat(_))


  // TODO : add ( in expression )
  def declareStatement: Parser[String] = declareOpt ~ sep ~ betweenBlockInNest(STATEMENT, hide = true) ^^ (concat(_))

  def statements: Parser[String] = (statement | declareStatement) ~ sep.? ~ rep((statement | declareStatement) ~ sep.? ^^
    (concat(_))) ^^ (concat(_)) // | statement ~ sep ~ statement

  // The code has some duplication for performance improvement in case of an often used  expression/statement , like the "if" for instance
  def statement: Parser[String] = skipOpt | ifNested(STATEMENT) | binaryExpression | lambdaFunction | betweenParInNested(STATEMENT) |
    labelCall | (functionCall ^^ (concat(_))) |
    nestCon(STATEMENT) | nestDec(VARIABLE) | anyAssignement | cellModification

  def expression: Parser[String] = ifNested(EXPRESSION) | binaryExpression | lambdaFunction | betweenParInNested(EXPRESSION) | labelCall |
    (functionCall ^^ (concat(_))) | anyAssignement | cellModification | expressionPrime

  def expressionPrime: Parser[String] = expressionPrimePrime | term

  def expressionPrimePrime: Parser[String] = functor(DOLLAR) | atExpression | underscore | lambdaFunction | binaryExpression | nestCon(EXPRESSION) | nestDec(DOLLAR) |
    dollar | variable


  def expressionForBinaryExp: Parser[String] = lambdaFunction | betweenParInNested(EXPRESSION) | labelCall | (functionCall ^^ (concat(_))) |
    atExpression | underscore | nestCon(EXPRESSION) | nestDec(DOLLAR) |
    dollar | variable | termP


  def nested(t: Alpha): Parser[String] = {
    t match {
      case STATEMENT => statements
      case EXPRESSION => expression
      case VARIABLE => variable
      case DOLLAR => dollarParser
      case PATTERN => pattern
      case METHOD_HEAD => variable | underscore | dollar
    }
  }

  def nested2(t: Alpha): Parser[String] = {
    t match {
      case STATEMENT => statement
      case EXPRESSION => expressionPrime
      case VARIABLE => variable
      case DOLLAR => dollarParser
    }
  }

  def nestedIn(t: Alpha): Parser[String] = {
    t match {
      case STATEMENT => inStatement
      case EXPRESSION => inExpression
      case EXPRESSION_OR_STATEMENT => (inStatement ~ sep ^^ (concat(_))).? ~ inExpression.? ^^ (concat(_))
    }
  }


  def betweenBlockInNest(t: Alpha, hide: Boolean = false, hideEnd: Boolean = false): Parser[String] = (openningBlockOpt ~ sep ~ ((valueAssignment | varAssignment) ~ sep ^^ (v => concat(v._1))).+ ~ ((sep ~ statement) ^^ (concat(_))).? ~
    sep.? ~ (nestedIn(t) | sep) ~ sep ~ closingBlockOpt ^^ {
    case _ ~ sep1 ~ values ~ stat ~ sep2 ~ alpha ~ sep5 ~ _ =>
      concat(orSpace(sep1), if (values.isEmpty) "" else if (hide) "" else localOpt + " ", values, if (values.isEmpty) "" else " in"
        , orSpace(sep1), stat, sep2, alpha, orSpace(sep5), if (hide || hideEnd) "" else "end")
  }) | (openningBlockOpt ~ sep ~ (nestedIn(t) | sep) ~ sep ~ closingBlockOpt ^^ {
    case _ ~ sep1 ~ exp ~ sep2 ~ _ => translate(orSpace(sep1), exp, orSpace(sep2))
  })

  def threadNest(t: Alpha): Parser[String] = threadOpt ~ sep ~ betweenBlockInNest(t, hide = true) ^^ {
    case opt ~ sep1 ~ codeBlock => translate(opt, if (sep1.isEmpty) " " else sep1, codeBlock, if (sep1.isEmpty) " " else sep1, closingBlockOpt)
  }

  def lockNest(t: Alpha): Parser[String] = lockOpt ~ sep ~ betweenParExpression.? ~ sep ~ betweenBlockInNest(t) ^^ {
    case opt ~ sep1 ~ exp ~ sep2 ~ codeBlock =>
      translate(opt, if (sep1.isEmpty) " " else sep1, exp, " then",
        if (sep2.isEmpty) " " else sep2, codeBlock, closingBlockOpt)
  }

  def nestConPrime(t: Alpha): Parser[String] = ifNested(t) | betweenBlockInNest(t) | atExpression | newCellExpression |
    caseNest(t) | exceptionNest(t) | threadNest(t) | lockNest(t) | raiseExpression | forLoop(t) /* | valueAssignment | varAssignment */
  //| lambdaFunction

  def nestCon(t: Alpha): Parser[String] = betweenParInNested(t) | nestConPrime(t)

  def nestDec(t: Alpha): Parser[String] = classNest(t) | functor(t) | procedure(t) | function(t)

  def unaryExpression: Parser[String] = unaryOperator ~ sep ~ expression ^^ {
    case op ~ sep1 ~ exp => translate(op, sep1, exp)
  }

  def binaryExpression: Parser[String] = expressionForBinaryExp ~ rep1(sep ~ binaryOperator ~ sep ~ (label | atom | expression) ^^ (concat(_))) ^^ {
    case op ~ list =>
      concat(op, list)
    //      list.foldLeft(op) {
    //        case (x, sep1 ~ o ~ sep2 ~ y) =>
    //          concat(x, sep1, o, sep2, y)
    //      }
  }


  def binaryExpression2: Parser[String] = (variable | dataExpression) ~ rep1(sep ~ binaryOperator ~ sep ~ (label | atom | expressionPrime)) ^^ {
    case op ~ list =>
      list.foldLeft(op) {
        case (x, sep1 ~ o ~ sep2 ~ y) =>
          concat(x, sep1, o, sep2, y)
      }
  }


  def binaryConsExp: Parser[String] = sep ~ (termP | variable | expressionPrimePrime) ~ rep1(sep ~ consBinOperator ~ sep ~ expression) ^^ {
    case sepL ~ op ~ list =>
      sepL + list.foldLeft(op) {
        case (x, sep1 ~ o ~ sep2 ~ y) =>
          translate(x, sep1, o, sep2, y)
      }
  }

  def binaryConsPattern: Parser[String] = sep ~ patternP ~ rep1(sep ~ consBinOperator ~ sep ~ pattern) ^^ {
    case sepL ~ op ~ list =>
      sepL + list.foldLeft(op) {
        case (x, sep1 ~ o ~ sep2 ~ y) =>
          translate(x, sep1, o, sep2, y)
      }
  }

  //(expression ~ sep).+
  def inExpression: Parser[String] = (statements ~ sep ~ expression ^^ {
    case stat ~ sep1 ~ exp =>
      translate(stat, sep1, exp)
  }) | expression

  def inStatement: Parser[String] = statements // ~ sep ^^ { case stat ~ sep1 => translate(stat, sep1) }

  def interactiveStatement: Parser[String] = realSep ~ rep((comments | statements | expression | valueAssignment | varAssignment) ~ realSep ^^ (concat(_))) ^^ {
    concat(_)
  }

}

object NewOzParserObj extends NewOzParser {

  def main(args: Array[String]): Unit = {
  }

  def parseContent(p: Parser[Any], content: String): Boolean = {
    parseContent(p, content, null)
  }

  def parseContent(p: Parser[Any], content: String, fileName: String): Boolean = {
    //Using phrase to consume all input
    parseAll(p, content) match {
      case default =>
        println(default.toString)
        default match {
          case Success(matched, x: CharSequenceReader) =>
            if (fileName != null) {
              new PrintWriter(fileName) {
                write("" + matched)
                close()
              }
            } else {
              val nl = if ((x.source + "").contains("\n") || (matched + "").contains("\n")) "\n" else ""
            }
            return true
          case Failure(msg, x: CharSequenceReader) => println(s"FAILURE: $msg ;couldn't translate ${""}")
          case Error(msg, _) => println(s"ERROR: $msg")
        }
    }
    false
  }

  def parseContent(content: String): Boolean = {
    parseContent(content, null)
  }

  def parseContent(content: String, filename: String): Boolean = {
    parseContent(interactiveStatement ^^ (default => concat(default)), content, filename)
  }

}