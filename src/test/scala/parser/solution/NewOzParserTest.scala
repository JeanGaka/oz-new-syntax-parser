package parser.solution

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import parser.solution.NewOzParserObj.{atomDelimiter, binaryExpression, casePatternThenNest, declareStatement, digit, expression, hexDigit, lispOpt, meth, methHead, methHeadSignature, methodHeadName, nzdigit, octDigit, ozCharacter, parseAll, parseContent, pattern, pseudoChar, stringChar}
import parser.solution.newOzParser.enums.NestableType._

class NewOzParserTest extends AnyFlatSpec with Matchers {
  // Atoms


  "An primitive parsing" should "should pass those tests" in {
    parseContent(digit, "0") should be(true)
    parseContent(digit, "9") should be(true)

    parseContent(hexDigit, "0") should be(true)
    parseContent(hexDigit, "a") should be(true)

    parseContent(octDigit, "7") should be(true)
    parseContent(octDigit, "0") should be(true)

    parseContent(nzdigit, "1") should be(true)
    parseContent(nzdigit, "9") should be(true)

    parseContent(pseudoChar, "\\000") should be(true)
    parseContent(pseudoChar, "\\xFF") should be(true)
    parseContent(pseudoChar, "\\a") should be(true)
    parseContent(pseudoChar, "\\\\") should be(true)
    parseContent(pseudoChar, "\\\"") should be(true)


    parseContent(stringChar, "a") should be(true)
    parseContent(stringChar, "\t") should be(true)

    parseContent(ozCharacter, "0") should be(true)
    parseContent(ozCharacter, "9") should be(true)
    parseContent(ozCharacter, "100") should be(true)
    parseContent(ozCharacter, "200") should be(true)
    parseContent(ozCharacter, "249") should be(true)
    parseContent(ozCharacter, "255") should be(true)

  }
  "An primitive parsing" should "should fail those tests" in {
    parseContent(digit, "a") should be(false)
    parseContent(digit, "11") should be(false)

    parseContent(hexDigit, "r") should be(false)

    parseContent(octDigit, "8") should be(false)

    parseContent(nzdigit, "0") should be(false)
    parseContent(nzdigit, "a") should be(false)

    parseContent(stringChar, "\\0") should be(false)
    parseContent(stringChar, "\"") should be(false)
    parseContent(stringChar, "\\") should be(false)

    parseContent(ozCharacter, "256") should be(false)
  }

  "An atom parsing" should "should pass those tests" in {
    parseContent(atomDelimiter + "ac" + atomDelimiter) should be(true)
    parseContent(lispOpt + "a_b") should be(true)
    parseContent(atomDelimiter + "\\´" + atomDelimiter) should be(true)
    parseContent(atomDelimiter + "\\xE3" + atomDelimiter) should be(true)
    parseContent(atomDelimiter + "\\xE3" + atomDelimiter) should be(true)
    parseContent(atomDelimiter + "\\´\\xE3" + atomDelimiter) should be(true)
    parseContent(atomDelimiter + "a" + atomDelimiter) should be(true)
    parseContent(atomDelimiter + "file:///home/mydir/FileOps.ozf" + atomDelimiter) should be(true)
  }

  "An atom parsing" should "should pass those tests by not being accepted" in {
    parseContent("'a'") should be(false)
    parseContent("'a_\\b") should be(false)
    parseContent(atomDelimiter + "\\´\\xE3" + atomDelimiter + atomDelimiter) should be(false)
  }


  // Value, variables and primitives
  "A variable/value parsing" should "should pass those tests" in {
    parseContent("3") should be(true)
    parseContent("var x = 3") should be(true)
    parseContent("x = @x") should be(true)
    parseContent("x := @x") should be(true)
    parseContent("c := y") should be(true)
    parseContent("val x = 3") should be(true)
    parseContent("val x = _") should be(true)
    parseContent("val X = _") should be(true)
    parseContent("X") should be(true)
    parseContent("x") should be(true)
    parseContent("'F") should be(true)
  }

  "A term parsing" should "should pass those tests" in {
    parseContent("A#A") should be(true)
    parseContent("A::A") should be(true)
    parseContent("[1]") should be(true)
    parseContent("[1,2,nil]") should be(true)
    parseContent("[1,2,[3,4]]") should be(true)
    parseContent("'person(1:X1,2:X2)") should be(true)
    parseContent("´|´(1:H,2:T)") should be(true)
    parseContent("´#´(1:H,2:T)") should be(true)
    //    parseContent(term,"A|A")
    //    parseContent(term,"[1]|A")
    //    parseContent(term,"!A|A")
    //    parseContent(term,"A|!A")
  }

  "A pattern parsing" should "should pass those tests" in {
    parseContent(pattern, "!A") should be(true)
    parseContent(pattern, "A") should be(true)
    parseContent(pattern, "A::A") should be(true)
    parseContent(pattern, "[A,!A]") should be(true)
    parseContent(pattern, "'label(!A)") should be(true)
    parseContent(pattern, "'label(!A,B)") should be(true)
    parseContent(pattern, "'label(!A,B, ...)") should be(true)
  }

  "A String parsing" should "should pass those tests" in {
    parseContent("\"this is a sTRING\"") should be(true)
  }

  //

  // Testing conditionals (if etc...)
  "A binary expression parsing" should "should pass those tests" in {
    parseContent(binaryExpression, "this") should be(false)
    parseContent(binaryExpression, "this + that + them") should be(true)
    parseContent(binaryExpression, " A ") should be(false)
  }
  "A assignement/modificaiton parsing" should "should pass those tests" in {
    parseContent("S = X") should be(true)
    parseContent("S := X") should be(true)
    parseContent("S.I := X") should be(true)
    parseContent("(S.I)") should be(true)
    parseContent("(S.I):= X") should be(true)
    parseContent("S.I:= X") should be(true)
  }
  "A conditional parsing" should "should pass those tests" in {
    parseContent("if(this != that){ 1+2 }") should be(true)
    parseContent("if(this != that){ 1+2 }") should be(true)
    parseContent("if(A != that){ \n\t1+2 \n}") should be(true)
    parseContent("if(A != that){ \n\t1+2 \n}") should be(true)
    //    parseContent("if(A != that){ \n\t1+2 \n} else if( this != that ) { 1+ 1 == 2}") should be(true)
    parseContent("if(this == that){ 1+2 } \nelse if( this != that ) { 1+ 1 == 2}") should be(true)
    parseContent("if(this == that){\n\t1+2 \n}else if( this != that )" +
      "{\n\t 1+ 1 == 2\n}else if( this != those ) {\n\t 1+ 1 == 2}") should be(true)
    parseContent("if(1 + 1 == 2){ 1+2 } else {1+3}") should be(true)
  }


  // Testing conditionals switch case
  "A switch case parsing" should "should pass those tests" in {
    parseContent("true") should be(true)
    parseContent("case p1 => {\n\ts1 \n}") should be(false)
    parseContent("case p1 && p2 => {\n\ts1 \n}") should be(false)
    parseContent(casePatternThenNest(EXPRESSION), "case p1 => {\n\ts1 \n}") should be(true)
    parseContent(casePatternThenNest(EXPRESSION), "case p1 => \n\ts1 \n") should be(true)
    parseContent("match elem {case p2 => {s2}}") should be(true)
    parseContent("match elem {case p2 && p3 => {s2}}") should be(true)
    parseContent("match elem {case p1 => {s1} case p2 => {s2}}") should be(true)
    parseContent("match elem {case p1 => {s1} case p2 && p3 => {s2}}") should be(true)
    parseContent("cond elem {case p1 => {s1} case p2 => {s2}}") should be(true)
    parseContent("match elem {case p1 => s1 case p2 => s2}") should be(true)
    parseContent("match elem {case p1 => s1 case p2 => s2 else s3}") should be(true)
    // nested
    parseContent("match elem {case p1 => s1 case p2 => s2 else match elem2{case p3 => s3 else s4}}") should be(true)
    parseContent("match elem {case p1 => {s1} case p2 => s2 else s3}") should be(true)
  }

  // Functions
  "A function or function call parsing" should "should pass those tests" in {
    parseContent("Do()") should be(true)
    parseContent("Do(a)") should be(true)
    parseContent("Do(a,b)") should be(true)
    parseContent("po(a,b , _, _)") should be(true)
    parseContent("defproc exc(ta,b){}") should be(true)
    parseContent("def do(ta,b){}") should be(false) // can work since it is a keyword from Oz
    parseContent("def exc(ta,b){\n\tlol\n}") should be(true)
    parseContent("def lazy exc(ta,b){\n\tlol\n}") should be(true)
    parseContent("defproc po(ta,b){\n\tskip\n}") should be(true)
    parseContent("def kill(X,S){if(P1){S1(X)}}") should be(true)
    parseContent("browse(\"lol\")") should be(true)
    parseContent("browse(A)") should be(true)
    parseContent("browse(A)") should be(true)
    parseContent("browse(a)") should be(true)
    parseContent("browse(a,b)") should be(true)
    parseContent("browse(a)(b)") should be(true)
    parseContent("browse(a)(b,c)(d)(E,F)") should be(true)
    parseContent("browse(A)(B)(C)(D)(E)") should be(true)
    parseContent("browse(A(B))") should be(true)
    //    parseContent("(ta,b) => {\n\tlol\n}") should be(true)
    parseContent(expression, "(ta,b) => {\n\tlol\n}") should be(true)
    parseContent(expression, "(ta,b){\n\tlol\n}") should be(true)
    parseContent("val F = (ta,b) => {\n\tlol\n}") should be(true)
    parseContent("browse(A)(B)") should be(true)
    parseContent("def exc(){\n    browse(A)\n}") should be(true)
    parseContent("S#L2 = MergeSortAcc(L1,N)") should be(true)
    parseContent("fibo(N-1) + fibo(n-2)") should be(true)

  }


  // Functors

  "A functor parsing" should "should pass those tests" in {
    parseContent("functor X\nimport I\nexport E\n{}") should be(true)
    parseContent("functor X\n\timport I\n\timport Z\n\texport E\n\timport Y\n{}") should be(true)
    parseContent("functor X\nimport I\nexport E\n{}") should be(true)
    parseContent("functor ${skip}") should be(true)
    parseContent("F=functor ${skip}") should be(true)
    parseContent("functor X\nimport Browser import FO from ´file:///home/mydir/FileOps.ozf´ " +
      "{Browser.browse(FO.countLines(´/etc/passwd´)) }") should be(true)

    parseContent("functor X\nimport Browser import FO from ´file:///home/mydir/FileOps.ozf´ " +
      "{val C = \"/etc/passwd\";Browser.browse(FO.countLines(c)) }") should be(true)
    parseContent("functor $ import I export 'e:E {skip}") should be(true)
    parseContent("functor import I export E {}") should be(true)
    parseContent("functor X\nimport Browser('browse:Browse,'x:X)\nexport E\n{}") should be(true)
  }

  "An exception parsing" should "should pass those raise related tests" in {
    parseContent("raise{ UnknownException }") should be(true)
  }
  "An exception parsing" should "should pass those try/catch related tests" in {
    parseContent("try{\n    obj(m)\n    x=normal\n}catch{\n   case e => x='exception(e)\n}") should be(true)
  }


  //Comments
  "A comment parsing" should "should pass those tests" in {
    parseContent("// comment") should be(true)
    parseContent("//{Browser.browse(FO.countLines(´/etc/passwd´))}") should be(true)
    //multiLineComment
    parseContent("/* comment */")
    parseContent("/* comment " +
      " \n\t comment2 */") should be(true)
    parseContent("\n\t/* comment " +
      " \n\t comment2 */") should be(true)
    parseContent("\n\t/*\n comment " +
      " \n\t comment2 */") should be(true)
  }

  // Threads /lock
  "A thread/lock parsing" should "should pass those tests" in {
    parseContent("thread{skip}") should be(true)
    parseContent("lock(L){skip}") should be(true)
    parseContent("lock{skip}") should be(true)
  }

  // Loop
  "A loop parsing" should "should pass those tests" in {
    parseContent("for(A in 1..10){\n\tskip\n}") should be(true)
    parseContent("for(A in 1..10;C){\n\tskip\n}") should be(true)
    parseContent("for(A in B;C;D){\n\tskip\n}") should be(true)
    parseContent("for(A in B;C;D){\n\tskip\n}") should be(true)
    parseContent("for(I in 1..1000 collect:C){\n\tskip\n}") should be(true)
    parseContent("for(I in 1..1000 default:C){\n\tskip\n}") should be(true)
    parseContent("for(I in 1..(N-1)){ A.I(setSucc(A.(I+1)))}") should be(true)
    parseContent("for(I in 1..(N-1)){ A.I('setSucc(A.(I+1)))}") should be(true)
    parseContent("for(A#B in S){\n\tskip\n}") should be(true)
  }
  // Classe
  "A class parsing" should "should pass those tests" in {
    // Methods have to be parsed in context or else they will be taken as normal functions
    parseContent(meth, "def foo(a:A,b:B,c:C){skip}") should be(false)
    parseContent(meth, "def 'foo('a:A,'b:B,'c:C){skip}") should be(true)
    parseContent(meth, "def 'foo(A,B,C){skip}") should be(true)
    parseContent(meth, "def Foo(A,B,C){skip}") should be(true)
    parseContent(meth, "def Foo(A,B,'c:B=0) {skip}") should be(true)
    parseContent(meth, "def Foo(A,B,'c:B) =M{skip}") should be(true)
    parseContent(meth, "def !Foo(A,B,C){skip}") should be(true)
    parseContent(meth, "def 'foo(A,B,C){skip}") should be(true)
    parseContent(methHead, "'foo(A,B,C)") should be(true)
    parseContent(methHeadSignature, "(A,B,C)") should be(true)
    parseContent("class X{}") should be(true)
    parseContent("class X extends Y{}") should be(true)
    parseContent("class X extends Y,Z{}") should be(true)
    parseContent("class X{\n\tdef Foo(A,B,C){skip}\n}") should be(true)
    parseContent("class X{\n\tdef Foo(A,B,C){skip}\n}") should be(true)
    parseContent("class X{\n\tdef Foo(A,B,C){skip}\n}") should be(true)
    parseContent("class X{\n\tattr X \n\tattr y \n\tattr 'a:C\n\tdef Foo(A,B,C){skip}\n}") should be(true)
    parseContent("class X{\n\tattr X \n\tattr y \n\tattr 'a:C\n\tdef Foo(A,B,C){skip}\n}") should be(true)
    parseContent("class X{\n\tprop X \n\tprop y \n\tprop X\n\tdef Foo(A,B,C){skip}\n}") should be(true)
    parseContent("class X{\n\tattr X \n\tprop y \n\tattr 'a:C\n\tdef Foo(A,B,C){skip}\n}") should be(true)
    parseContent("class X{\n\tattr X \n\tprop y \n\tattr 'a:C\n\tdef Foo(A,B,C){skip}\n\tdef Foo2(A,B,C){skip}\n}") should be(true)
  }

  "A method head name parsing" should "pass those tests" in {
    parseContent(methodHeadName, "A") should be(true)
    parseContent(methodHeadName, "'b") should be(true)
  }

  "A method head name parsing" should "fail those tests" in {
    parseContent(methodHeadName, "a") should be(false)
    parseContent(methodHeadName, "'B") should be(false)
  }

  // Code block and local statements
  "A codeblock parsing" should "pass those tests" in {

    parseContent("{val A = 1,B;}") should be(true)
    parseContent("{val A = 1,B = 3 + 4; A + B}") should be(true)
    parseContent("{val X,Y,Z; A + B}") should be(true)
    parseContent("{\n\tval X,Y,Z;\n\t'f(X,'b)='f('a,Y)\n\t'f(Z,'a)=Z\n\tBrowse([X,Y,Z])\n}") should be(true)
    parseContent("{val 'tree(X, ...) = T\n Browse(T)}") should be(true)
    parseContent("{\n\tval A,B,C,D;\n\tLabel(T) = 'tree\n\tA=T.key\n\tB=T.left\n\tBrowse(T)}") should be(true)
    parseContent("{val A = 1,B;}") should be(true)
    parseContent("{var A = 1,B = 4;}") should be(true)
    parseContent("{var A = 1,B = 4;var C = 3;}") should be(true)
    parseContent("{}") should be(true)
    parseContent("{ 1+2 }") should be(true)
    parseContent("{ 1+2 }") should be(true)
  }

  "A declare statement paring" should "pass those test" in {
    parseContent(declareStatement, "declare{skip}") should be(true)
    parseContent(declareStatement, "declare {val A;skip}") should be(true)
  }


  // Bonus
  "Additionally, parsing" should "pass those tests" in {
    parseContent("if(n<2){ 1 }else{fibo(n-1) + fibo(n-2)}") should be(true)
    parseContent("fibo(N-1)") should be(true)
    parseContent(atomDelimiter + "ac" + atomDelimiter) should be(true)
    parseContent("{val x = 3; skip}") should be(true)
    parseContent("if(A == B){ 1+2 } \nelse if( A != C ) { 1+ 1}") should be(true)
    parseContent("thread{val s;newport(s,p)\nfor(m#x in s){\ntry{\n    obj(m)\n    x=normal\n}catch{\n   case e => x='exception(e)\n}\n            }}") should be(true)
    parseContent("def fibo(n){skip  }") should be(true)
    parseContent("class ComputeServer{\n\t    def init(){skip}\n\t    def run(p){p()}\n    }") should be(true)
    parseContent("'exception(e)") should be(true)
    parseContent("class BallGame{\nattr other\nattr count:0\n\ndef 'init(Other){\n  other:=Other\n}\ndef 'ball(){\n  count:=@count+1\n  @other('ball)\n}\ndef 'get(x){\n  x=@count\n}\n    }") should be(true)
    parseContent("/* Execution */\ndefproc building(fn,ln, out,floors,lifts){\n    lifts=maketuple(lifts,ln)\n    for (i in 1..ln) {\n val cid;\n cid=controller('state(stopped,1,lifts.i))\n lifts.i=lift(i,'state(1,nil,false),cid,floors)\n    }\n    floors=maketuple(floors,fn)\n    for (i in 1..fn) {\n floors.i=floor(i,'state(notcalled),lifts)\n    }\n}") should be(true)

  }
}
