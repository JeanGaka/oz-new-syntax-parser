
name := "NewOzParser"

version := "release-v1.2.2"

mainClass in assembly := Some("NewOzCompiler")
scalaVersion := "2.12.6"
discoveredMainClasses in Compile += "main.scala.parser.TestSimpleOzParser"

libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "1.1.2"

libraryDependencies += "org.scala-lang" % "scala-compiler" % "2.10.2-RC2"
//For testing
libraryDependencies += "org.scalactic" %% "scalactic" % "3.1.2"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.1.2" % "test"
//resolvers += "Artima Maven Repository" at "http://repo.artima.com/releases"
enablePlugins(SbtProguard)

proguardOptions in Proguard ++= Seq("-dontnote", "-dontwarn", "-ignorewarnings")

proguardOptions in Proguard += ProguardOptions.keepMain("NewOzCompiler")