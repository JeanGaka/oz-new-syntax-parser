# Introduction
newOz parser and compiler meant to extend/decorate the official Oz compiler and allow writing  and executing
newOz code.

## Required Software
<ul>
    <li>[Oz](https://sourceforge.net/projects/mozart-oz/) installed</li>
    <li>Java 8+ installed</li>
    <li>Scala and SBT if you want to develop on the repo (Optional)</li>
</ul>

## Releases download 
https://www.dropbox.com/sh/w9zll8jcz7p8cz5/AADsZ76JfmZEcnMoKmPDKVyHa?dl=0

## Usage
`NewOzParser.jar { option | nozFile | directory}*`

Additionally from the normal Oz compiler arguments, newOz provides the following ones :
<ul>
<li>*-C,--clean Clean the intermediary files (default mode).</li>
<li>*-k,--keep Keeps the intermediary files.</li>
<li>*-f,--folder Input and Output Folder to use (current folder if nothing),will compile all the files inside.</li>
<li>*-F,--force Displays newOz compiler message.</li>
<li>*-v,--verbose Displays newOz compiler message.</li>
<li>*-r,--run Runs the first (left-most) newOz file it with ozengine command.</li>
</ul>

